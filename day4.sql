-- mysql -h 34.87.139.140 -u student -p
-- use password student

USE practice;
ALTER TABLE result ADD FOREIGN KEY (student_id) REFERENCES student(id);

-- insert your student and result data

SELECT max(marks_obtained) FROM result WHERE student_id='076BIM443';
SELECT min(marks_obtained) FROM result WHERE student_id='076BIM443';

SELECT count(marks_obtained) FROM result WHERE marks_obtained > pass_mark;

-- other aggregate functions sum(), average()
