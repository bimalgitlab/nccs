-- download and install xampp
-- start mysql from xampp control panel
-- add mysql/bin path to environment path
-- open command prompt
-- connect to mysql server using command 
--      mysql -u root -p
--   leave blank for password


SHOW DATABASES; -- list all available databases
CREATE DATABASE practice;
SHOW DATABASES;
USE practice; -- connect to a database

CREATE TABLE product (name VARCHAR(100), price INT, quantity FLOAT, unit VARCHAR(10));
SHOW tables;
DESCRIBE product;

INSERT INTO product VALUES
    ('Jira masino rice', 2000, 50, '25kg'),
    ('Olive oil', 1500, 20, '1lX12bag'),
    ('Wai wai Noodle', 20, 200, '');
SELECT * FROM product;
UPDATE product SET quantity = 49 WHERE name = 'Jira masino rice';
SELECT * FROM product;
DELETE FROM product WHERE name = 'Olive oil';
SELECT * FROM product;

EXIT;
